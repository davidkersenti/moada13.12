import { Component, OnInit } from '@angular/core';
import {Post} from './post'

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {
post:Post;
  constructor() { }

  ngOnInit() {
  }

}
