import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';

import{AngularFireModule} from 'angularfire2';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserComponent } from './user/user.component';
import { ProductsComponent } from './products/products.component';
import {  ProductsService } from './products/products.service';
import { ProductComponent } from './product/product.component';
import { JoinsComponent } from './joins/joins.component';
import {  JoinsService } from './joins/joins.service';
import { JoinComponent } from './join/join.component';

import { PostsComponent } from './posts/posts.component';
import {  PostsService } from './posts/posts.service';

import { CategoriesComponent } from './categories/categories.component';
import { PostComponent } from './post/post.component';
import { InvoicesComponent } from './invoices/invoices.component';
import {  InvoicesService } from './invoices/invoices.service';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';





  
export const firebaseConfig = {
   apiKey: "AIzaSyAybWqWd2Dbr6A6TPi3aZF-t3l5kfSAPoo",
    authDomain: "users-352f2.firebaseapp.com",
    databaseURL: "https://users-352f2.firebaseio.com",
    projectId: "users-352f2",
    storageBucket: "users-352f2.appspot.com",
    messagingSenderId: "914352984198"
}

const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'userForm', component: UsersComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'joins', component: JoinsComponent },
  { path: 'invoices', component: InvoicesComponent },
  { path: 'invoiceForm', component: InvoiceFormComponent },
  { path: 'posts', component: PostsComponent },
  { path: '', component: InvoicesComponent },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
     PageNotFoundComponent,
     UserComponent,
      UserFormComponent,
      ProductsComponent,
      ProductComponent,
      JoinsComponent,
      JoinComponent,
     
      PostsComponent,
      CategoriesComponent,
      PostComponent,
      InvoicesComponent,
      InvoiceFormComponent,
      InvoiceComponent,
      SpinnerComponent,
   

  
  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
   AngularFireModule.initializeApp(firebaseConfig),
    RouterModule.forRoot(appRoutes)

  ],
  providers: [UsersService,ProductsService,JoinsService,PostsService,InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
