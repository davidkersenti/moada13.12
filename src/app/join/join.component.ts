import { Component, OnInit } from '@angular/core';
import {Join} from './join'

@Component({
  selector: 'app-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.css'],
   inputs:['join']
})
export class JoinComponent implements OnInit {
  join:Join;

  constructor() { }

  ngOnInit() {
  }

}
