/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { JoinsService } from './joins.service';

describe('JoinsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JoinsService]
    });
  });

  it('should ...', inject([JoinsService], (service: JoinsService) => {
    expect(service).toBeTruthy();
  }));
});
