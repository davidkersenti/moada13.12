import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';


@Injectable()
export class JoinsService {

   joinsObservable;

 constructor(private af:AngularFire) { }

 getJoins(){
    this.joinsObservable = this.af.database.list('/joins').map(
      joins =>{
        joins.map(
          join => {
            join.categoriesTitles = [];
            for(var p in join.categories){
                join.categoriesTitles.push(
                this.af.database.object('/joins/' + p)
              )
            }
          }
        );
        return joins;
      }
    )
    //this.usersObservable = this.af.database.list('/users');
    return this.joinsObservable;
	}

}
