import { Component, OnInit } from '@angular/core';
import {JoinsService} from './joins.service';

@Component({
  selector: 'app-joins',
  templateUrl: './joins.component.html',
  styleUrls: ['./joins.component.css']
})
export class JoinsComponent implements OnInit {

  joins;
  currentJoins;
  isLoading = true;

   constructor(private _joinssService: JoinsService) {
    //this.users = this._userService.getUsers();
  }

 ngOnInit() {
        this._joinssService.getJoins()
			    .subscribe(joins => {this.joins = joins;
                               this.isLoading = false});
  }
}
